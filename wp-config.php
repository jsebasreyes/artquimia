<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'art');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', ')tZ,K[ >0JnTyjQ=31am_@ApV;TKhK{s% 6?h=y1=>EWqvzZ+KquNi0H5XP[$>XG');
define('SECURE_AUTH_KEY', '>E!#!kkV%j0fNT)uolG<E0;,!`@&JP2Yqh-%Y48]B6Erc*5pp`)hU!o)1]G)W.*A');
define('LOGGED_IN_KEY', '?Hh%Dq5z~7#?{R9%)IqF+p;<8Z5:i#MSS,V5WOeTd~9iQ)tu!EP+}JB^Pi?TeS8L');
define('NONCE_KEY', '$bKx<#P$//qX0:ARC]%f26*8Q&(J979W2M^rN_TCEclCU? rvCFow;Y$$k$nw|xV');
define('AUTH_SALT', 'DP;fR?1B93DKl`<P-doDj~DT:>SuJ-~<p9V4>hhlgA$!:JHh{o1b%tE+0< [0l~M');
define('SECURE_AUTH_SALT', 'WbDq/+$w*<z_a83A{(R3?=1Q>z;XyfFi66i8igB, xyXK_0!{8`lY 5I!=h%/#MH');
define('LOGGED_IN_SALT', 'pH:yll,$WN%1bvdYF!P<WQ,9cN<}5Lj0]h%ozS&d4Qwe8o256+vpaK-suU<{U})n');
define('NONCE_SALT', 'iXj(iao_J{-*|$JGkMN&:7KD&fg_)UBj;>H<$]}s_.g>u/qujh-RpJKAMAUgxHcQ');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

