<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if( !is_page_template( 'blank-page-with-container.php' )): ?>
		
	</div>
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="mifooter"  role="contentinfo" style="min-height: 8rem;     display: flex;
    align-items: center; text-align: center;">
		<!-- Shortcode Footer --><?php echo do_shortcode('[sc name="Footer"]'); ?><!-- Shortcode Footer -->
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>