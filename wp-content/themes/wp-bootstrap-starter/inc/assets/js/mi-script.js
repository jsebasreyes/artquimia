/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery( function ( $ ) {
    'use strict';
     
   
    window.addEventListener('load', function (){
       
       	
	// init controller
	var controller = new ScrollMagic.Controller();
	// build tween
        var tween = TweenMax.to(".miimg", 0.5, {scale: 1.2, y:200, ease: Linear.easeNone});
	// build scene
	var scene = new ScrollMagic.Scene({triggerElement: ".animacion", duration: 300, offset: 50})
	.setTween(tween)
	.addIndicators({name: "resize"}) // add indicators (requires plugin)
        .addTo(controller);
    });
    
});


